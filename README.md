# Arbitrand Driver

This repository is a fork of the Xilinx XDMA driver with a few Arbitrand-specific changes.

Changes:
- Device ID set changed to only the Arbitrand device ID
- Module name changed to "arbitrand" from "xdma"
- Makefile installs the module creates symlinks from /dev/arbitrand[n] to the nth FPGA

More significant changes are planned for the future.